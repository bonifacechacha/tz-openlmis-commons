FROM openlmis/service-base:4

ARG GA

COPY build/libs/*.jar /service.jar
COPY src/main/resources/db/demo-data/*.csv /demo-data/
COPY build/schema /schema
COPY build/consul /consul

RUN echo "${GA}" > /ga.json