/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.repository;

import java.util.UUID;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

@Repository
public class ProcessingPeriodRepository {

  @PersistenceContext
  private EntityManager entityManager;

  /**
   * Verify that the processing period of the specified id exist.
   *
   * @param periodId id of processing period to verify.
   */
  public void verifyProcessingPeriodExists(UUID periodId) {
    String existSql = "select count(id) from referencedata.processing_periods where id='"
        + periodId
        + "'";
    Object existing = entityManager.createNativeQuery(existSql).getSingleResult();
    if (Long.valueOf(existing.toString()) <= 0L) {
      throw new IllegalArgumentException("There is no processing period with id: " + periodId);
    }
  }

  /**
   * Count requisition associated with the processing period of the specified id.
   *
   * @param periodId id of processing period to count requisitions for.
   */
  public Long countProcessingPeriodRequisitions(UUID periodId) {
    String existSql = "select count(id) from requisition.requisitions where processingperiodid='"
        + periodId
        + "'";
    Object existing = entityManager.createNativeQuery(existSql).getSingleResult();
    return Long.valueOf(existing.toString());
  }

  /**
   * Delete the processing period of the specified id.
   *
   * @param periodId id of processing period to delete.
   */
  public void deleteProcessingPeriod(UUID periodId) {
    String deleteSql = "delete from referencedata.processing_periods where id='" + periodId + "'";
    entityManager.createNativeQuery(deleteSql).executeUpdate();
  }
}
