/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.web;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.openlmis.common.domain.IntegrationLog;
import org.openlmis.common.dto.IntegrationLogDto;
import org.openlmis.common.dto.referencedata.FacilityDto;
import org.openlmis.common.dto.referencedata.MinimalFacilityDto;
import org.openlmis.common.dto.referencedata.ProgramDto;
import org.openlmis.common.dto.referencedata.SupportedProgramDto;
import org.openlmis.common.dto.requisition.FacilityReportingPeriodDto;
import org.openlmis.common.dto.requisition.PeriodDto;
import org.openlmis.common.dto.requisition.ProcessingPeriodDto;
import org.openlmis.common.dto.requisition.ReportingPeriodDto;
import org.openlmis.common.dto.requisition.ReportingPeriodSearchCriteriaDto;
import org.openlmis.common.dto.requisition.RequisitionDto;
import org.openlmis.common.dto.requisition.RequisitionDtoV2;
import org.openlmis.common.dto.requisition.RequisitionParamDto;
import org.openlmis.common.dto.requisition.response.RequisitionSummaryDto;
import org.openlmis.common.dto.requisition.response.ResponseDto;
import org.openlmis.common.exception.ValidationMessageException;
import org.openlmis.common.i18n.MessageKeys;
import org.openlmis.common.repository.IntegrationLongRepository;
import org.openlmis.common.service.referencedata.FacilityReferenceDataService;
import org.openlmis.common.service.referencedata.ProgramReferenceDataService;
import org.openlmis.common.service.requisition.Requisition2Service;
import org.openlmis.common.service.requisition.RequisitionService;
import org.openlmis.common.util.Pagination;
import org.slf4j.ext.XLogger;
import org.slf4j.ext.XLoggerFactory;
import org.slf4j.profiler.Profiler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
@Transactional
@RequestMapping("/api/rest-api")
public class RestApiController extends BaseController {

  private static final XLogger XLOGGER = XLoggerFactory.getXLogger(ReportsController.class);

  @Autowired
  private FacilityReferenceDataService facilityReferenceDataService;

  @Autowired
  RequisitionService requisitionService;

  @Autowired
  private Requisition2Service requisition2Service;

  @Autowired
  ProgramReferenceDataService programReferenceDataService;

  @Autowired
  private IntegrationLongRepository integrationLongRepository;

  /**
   * Get periods for facility or facilities requisition.
   *
   * @return the list of periods available for facilities.
   */
  @Transactional(isolation = Isolation.SERIALIZABLE)
  @PostMapping("/reportingPeriod")
  public ResponseEntity<FacilityReportingPeriodDto> create(
      @RequestBody ReportingPeriodSearchCriteriaDto
          criteriaDto,
      BindingResult bindingResult) {

    Profiler profiler = new Profiler("REPORTING_PERIOD_CREATE");
    profiler.setLogger(XLOGGER);

    profiler.start("VALIDATE_SEARCH_CRITERIA");
    XLOGGER.info("GETTING THE DATA FROM UI");
    XLOGGER.debug("{}=", criteriaDto);

    FacilityReportingPeriodDto periodDto = new FacilityReportingPeriodDto();
    periodDto.setSourceApplication(criteriaDto.getSourceApplication());

    List<MinimalFacilityDto> facilityList
        = facilityReferenceDataService.search(criteriaDto.getHfrIds().get(0), null, null, false);

    if (!facilityList.isEmpty()) {

      List<PeriodDto> periodList = new ArrayList<>();

      for (MinimalFacilityDto facilityDto : facilityList) {

        FacilityDto facility = facilityReferenceDataService.findOne(facilityDto.getId());

        PeriodDto periodDtoList = new PeriodDto();

        List<RequisitionParamDto> programs = new ArrayList<>();

        for (SupportedProgramDto programDto : facility.getSupportedPrograms()) {

          List<RequisitionDto> requisitions =
              requisitionService.search4(
                  facility.getId(), programDto.getId(),
                  criteriaDto.getEmergency()
              );

          RequisitionParamDto prog = new RequisitionParamDto();
          prog.setProgramName(programDto.getName());
          prog.setProgramCode(programDto.getCode());

          if (!requisitions.isEmpty()) {

            List<ReportingPeriodDto> per = new ArrayList<>();
            for (ProcessingPeriodDto periodDto1 : requisitions) {

              ReportingPeriodDto dto = new ReportingPeriodDto();

              dto.setId(periodDto1.getId());
              dto.setName(periodDto1.getName());
              dto.setScheduleId(periodDto1.getProcessingSchedule().getId());
              dto.setDescription(periodDto1.getName());
              dto.setNumberOfMonths(periodDto1.getDurationInMonths());
              dto.setStartDate(LocalDate.parse(periodDto1.getStartDate()));
              dto.setEndDate(LocalDate.parse(periodDto1.getEndDate()));
              dto.setProcessingScheduleName(periodDto1.getProcessingSchedule().getName());
              per.add(dto);

            }
            prog.setReportingPeriods(per);

          }

          programs.add(prog);
        }

        periodDtoList.setPrograms(programs);
        periodDtoList.setHfrId(facilityDto.getCode());
        periodList.add(periodDtoList);
      }
      periodDto.setFacilities(periodList);
    }

    XLOGGER.info("Facilities");
    profiler.stop().log();

    return ResponseEntity.ok().body(periodDto);
  }

  /**
   * Initiate requisition.
   *
   * @return the requisition was initiated.
   */
  @Transactional(isolation = Isolation.SERIALIZABLE)
  @PostMapping("/initiate")
  public ResponseEntity<RequisitionSummaryDto> create(
      @RequestBody RequisitionParamDto param,
      BindingResult bindingResult) {

    Profiler profiler = new Profiler("REQUISITION_INITIATE_CREATE");
    profiler.setLogger(XLOGGER);

    profiler.start("VALIDATE_REQUISITION");
    XLOGGER.info("GETTING THE DATA FROM UI");

    List<ProgramDto> programList = programReferenceDataService.findAll();

    Optional<ProgramDto> result = programList
        .stream().parallel()
        .filter(program -> program.getCode().equalsIgnoreCase(param.getProgramCode())).findAny();

    List<MinimalFacilityDto> facilityList
        = facilityReferenceDataService.search(param.getHfrId(), null, null, false);

    RequisitionSummaryDto summary = null;

    if (result.isPresent() && !facilityList.isEmpty()) {

      XLOGGER.info("Initiate requisition");
      summary = requisitionService.initiate(result.get().getId(),
          facilityList.get(0).getId(), param.getEmergency(), param.getPeriodId());

    } else {
      XLOGGER.info("Program or Facility not present");
      return ResponseEntity.badRequest().body(null);
    }

    profiler.stop().log();

    return ResponseEntity.ok().body(summary);
  }

  /**
   * Submits earlier initiated requisition.
   */
  @PostMapping("/submit")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public RequisitionSummaryDto submitRequisition(
      @RequestBody RequisitionSummaryDto requisition,
      HttpServletRequest request,
      HttpServletResponse response) {

    Profiler profiler = new Profiler("REQUISITION_SUBMIT");
    profiler.setLogger(XLOGGER);
    RequisitionDtoV2 dbReport = null;
    if (requisition != null) {

      XLOGGER.info("Get Initiated requisition");
      dbReport = requisition2Service.findOne(requisition.getRnrId());
      if (dbReport != null) {
        XLOGGER.info("Update data element and submit the requisition");
        requisition2Service.submit(dbReport, requisition);

      } else {
        throw new ValidationMessageException(MessageKeys.NOT_FOUND);
      }
    } else {
      throw new ValidationMessageException(MessageKeys.ERROR_MISSING_MANDATORY_FIELD);
    }
    profiler.stop();

    return requisition;

  }

  /**
   * Get requisition status requisition.
   *
   * @return the last approval stage of requisition.
   */
  @Transactional(isolation = Isolation.SERIALIZABLE)
  @GetMapping("/requisition-status")
  public ResponseEntity<ResponseDto> getRequisitionStatusChanges(
      @RequestBody ReportingPeriodSearchCriteriaDto
          criteriaDto) throws Exception {

    Profiler profiler = new Profiler("REPORTING_REQUISITION_STATUS_CHANGES");
    profiler.setLogger(XLOGGER);

    profiler.start("VALIDATE_SEARCH_CRITERIA");

    RequisitionDtoV2 requisition = null;

    if (criteriaDto.getRequisitionId() != null) {

      XLOGGER.info("GETTING THE DATA FROM API");
      requisition = requisition2Service.findOne(criteriaDto.getRequisitionId());

    }

    XLOGGER.debug("{}=", requisition);
    ResponseDto response = null;

    XLOGGER.info("prepare response to be sent");

    if (requisition != null) {
      response = new ResponseDto();
      response.setStatus(requisition.getStatus());
      response.setCreatedDate(requisition.getCreatedDate());
    } else {

      throw new Exception(MessageKeys.ERROR_REQUISITION_NOT_FOUND, null);

    }

    profiler.stop().log();

    return ResponseEntity.ok().body(response);
  }

  /**
   * Retrieve OpenLMIS integration report.
   *
   * @return all integration logs.
   */
  @GetMapping("/integrationLogs")
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public Page<IntegrationLogDto> getLogReport(Pageable pageable) {

    Page<IntegrationLog> list = integrationLongRepository.findAll(pageable);

    List<IntegrationLogDto> logs = list.getContent()
        .stream()
        .map(IntegrationLogDto::newInstance)
        .collect(Collectors.toList());

    return Pagination.getPage(logs, pageable, logs.size());
  }

}
