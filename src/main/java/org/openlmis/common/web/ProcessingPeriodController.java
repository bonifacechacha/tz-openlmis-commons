/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.web;

import static org.openlmis.common.web.ProcessingPeriodController.RESOURCE_PATH;

import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.openlmis.common.service.ProcessingPeriodService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
@RequiredArgsConstructor
@RequestMapping(RESOURCE_PATH)
public class ProcessingPeriodController extends BaseController {

  public static final String RESOURCE_PATH = BaseController.API_PATH + "/tzProcessingPeriods";

  private final ProcessingPeriodService processingPeriodService;

  /**
   * Delete processing period of specified id.
   *
   * @param periodId id of the processing period to be deleted.
   */

  @ResponseStatus(HttpStatus.OK)
  @DeleteMapping(value = "/{id}")
  public void deleteProcessingPeriod(@PathVariable("id") UUID periodId) {
    processingPeriodService.deleteProcessingPeriod(periodId);
  }
}
