/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.web;

import java.io.IOException;
import java.util.UUID;
import javax.servlet.http.HttpServletResponse;
import org.openlmis.common.dto.fulfillment.OrderDto;
import org.openlmis.common.dto.referencedata.FileTemplateDto;
import org.openlmis.common.service.fulfillment.FileTemplateService;
import org.openlmis.common.service.fulfillment.OrderFulfillmentService;
import org.openlmis.common.service.fulfillment.TzOrderCsvHelper;
import org.slf4j.ext.XLogger;
import org.slf4j.ext.XLoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
@Transactional
@RequestMapping("/api/exportOrders")
public class ReportsController extends BaseController {

  private static final XLogger XLOGGER = XLoggerFactory.getXLogger(ReportsController.class);
  private static final String DISPOSITION_BASE = "attachment; filename=";
  private static final String TYPE_CSV = "csv";

  @Autowired
  private TzOrderCsvHelper tzOrderCsvHelper;

  @Autowired
  private FileTemplateService fileTemplateService;

  @Autowired
  private OrderFulfillmentService orderFulfillmentService;

  /**
   * Exporting order to csv.
   *
   * @param orderId  UUID of order to print
   * @param type     export type
   * @param response HttpServletResponse object
   */
  @RequestMapping(value = "/{id}/export", method = RequestMethod.GET)
  @ResponseStatus(HttpStatus.OK)
  public void export(@PathVariable("id") UUID orderId,
                     @RequestParam(value = "type", required = false,
                         defaultValue = TYPE_CSV) String type,
                     HttpServletResponse response) throws IOException {
    if (!TYPE_CSV.equals(type)) {
      String msg = "Export type: " + type + " not allowed";
      XLOGGER.warn(msg);
      response.sendError(HttpServletResponse.SC_BAD_REQUEST, msg);
      return;
    }

    OrderDto order = orderFulfillmentService
        .findOne(orderId);

    if (order == null) {
      String msg = "Order does not exist.";
      XLOGGER.warn(msg);
      response.sendError(HttpServletResponse.SC_NOT_FOUND, msg);
      return;
    }

    FileTemplateDto fileTemplate = fileTemplateService.findOrderTemplate();

    if (fileTemplate == null) {
      String msg = "Could not export Order, because Order Template File not found";
      XLOGGER.warn(msg);
      response.sendError(HttpServletResponse.SC_NOT_FOUND, msg);
      return;
    }

    XLOGGER.debug("{}=", fileTemplate);

    response.setContentType("text/csv");
    response.addHeader(HttpHeaders.CONTENT_DISPOSITION,
        DISPOSITION_BASE + fileTemplate.getFilePrefix() + order.getOrderCode() + ".csv");

    try {
      tzOrderCsvHelper.writeCsvFile(order, fileTemplate, response.getWriter());
    } catch (IOException ex) {
      response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
          "Error occurred while exporting order to csv.");
      XLOGGER.error("Error occurred while exporting order to csv", ex);
    }
  }

}
