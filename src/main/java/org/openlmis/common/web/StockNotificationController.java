/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.web;

import static org.openlmis.common.web.StockNotificationController.RESOURCE_PATH;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import javax.servlet.http.HttpServletResponse;
import org.openlmis.common.domain.StockNotification;
import org.openlmis.common.dto.StockNotificationDto;
import org.openlmis.common.dto.fulfillment.OrderDto;
import org.openlmis.common.exception.NotFoundException;
import org.openlmis.common.exception.ReportingException;
import org.openlmis.common.exception.ValidationMessageException;
import org.openlmis.common.i18n.MessageKeys;
import org.openlmis.common.repository.StockNotificationRepository;
import org.openlmis.common.service.JasperReportsViewService;
import org.openlmis.common.service.fulfillment.OrderFulfillmentService;
import org.openlmis.common.util.AuthenticationHelper;
import org.openlmis.common.util.DateConverterHelper;
import org.openlmis.common.util.Message;
import org.slf4j.ext.XLogger;
import org.slf4j.ext.XLoggerFactory;
import org.slf4j.profiler.Profiler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
@Transactional
@RequestMapping(RESOURCE_PATH)
public class StockNotificationController extends BaseController {

  public static final String RESOURCE_PATH = API_PATH + "/stockOutNotifications";

  private final XLogger extLogger = XLoggerFactory.getXLogger(getClass());

  @Autowired
  AuthenticationHelper authenticationHelper;

  @Autowired
  private OrderFulfillmentService orderFulfillmentService;

  @Autowired
  private StockNotificationRepository stockNotificationRepository;

  @Value("${groupingSeparator}")
  private String groupingSeparator;

  @Value("${groupingSize}")
  private String groupingSize;

  @Value("${dateTimeFormat}")
  private String dateTimeFormat;

  @Autowired
  private JasperReportsViewService jasperReportsViewService;

  @Autowired
  private DateConverterHelper dateConverterHelper;

  @Value("${validStockNotificationDays}")
  private Integer validStockNotificationDays;

  /**
   * Create a new stockoutNotification using the provided StockoutNotification DTO.
   *
   * @param notificationDto processing period DTO with which to create the processing period
   *
   * @return the new stock notification.
   */
  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  @ResponseBody
  public StockNotificationDto updateNotification(
      @RequestBody StockNotificationDto notificationDto) {
    Profiler profiler = getProfiler("SUBMIT_STOCK_NOTIFICATION", notificationDto);

    if (null == notificationDto.getRequisitionId()
        || null == notificationDto.getElmisOrderNumber()) {
      throw new ValidationMessageException(MessageKeys.MUST_CONTAIN_VALUE);
    }

    profiler.start("CHECK IF ORDERS EXISTS");

    findOrder(notificationDto, profiler);

    profiler.start("PREPARE TO UPDATE NOTIFICATION");

    StockNotification newNotification =
        StockNotification.newInstance(notificationDto);

    profiler.start("DB_CALL");
    StockNotification storedNotification = stockNotificationRepository
        .findByRequisitionId(notificationDto.getRequisitionId());

    if (storedNotification != null) {
      profiler.start("SET EXISTING ID FOR UPDATE");
      newNotification.setId(storedNotification.getId());
    }

    profiler.start("SAVE");

    newNotification = stockNotificationRepository.save(newNotification);

    profiler.stop().log();
    return StockNotificationDto.newInstance(newNotification);

  }

  /**
   * Retrieve OpenLMIS visitor report form Google Analytics account.
   * on specific period.
   *
   * @return all stock notifications.
   */
  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  @ResponseBody
  public Iterable<StockNotificationDto> getAll() {

    return StockNotificationDto.newInstance(stockNotificationRepository.findAll());

  }

  /**
   * Generate a report based on the template, the format and the request parameters.
   *
   * @param response request (to get the request parameters)
   */
  @RequestMapping(value = "/{id}/print", method = RequestMethod.GET)
  public void generateReport(@PathVariable("id") UUID orderId,
                             HttpServletResponse response,
                             Profiler profiler)
      throws ReportingException {

    profiler.start("GET_ORDER_BY_ID");

    OrderDto order = orderFulfillmentService
        .findOne(orderId);
    if (order == null) {
      throw new NotFoundException(
          new Message(MessageKeys.ERROR_ORDER_NOT_FOUND, orderId));
    }

    profiler.start("SET PARAMETERS");
    Map<String, Object> params = new HashMap<>();
    DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
    decimalFormatSymbols.setGroupingSeparator(groupingSeparator.charAt(0));
    DecimalFormat decimalFormat = new DecimalFormat("", decimalFormatSymbols);
    decimalFormat.setGroupingSize(Integer.parseInt(groupingSize));
    params.put("decimalFormat", decimalFormat);
    params.put("dateTimeFormat", dateTimeFormat);
    params.put("order", order);
    StockNotification notification =
        stockNotificationRepository
            .findByRequisitionId(order.getId());
    if (notification == null) {
      throw new NotFoundException(MessageKeys.NOT_FOUND);
    }

    StockNotificationDto dto = StockNotificationDto
        .newInstance(notification);

    params.put("datasource", dto.getLineItems());

    profiler.start("ADD PERIOD TO GET VALID ONE");

    params.put("periodValidity",
        dateConverterHelper
            .getFormattedDateFromString(dto.getProcessingDate())
            .plusDays(validStockNotificationDays).toString());

    params.put("stockNotification", dto);
    params.put("titleFullFilledItems", "Stock Out Items");

    params.put("loggedInUser", authenticationHelper.getCurrentUser());
    jasperReportsViewService.generateNotification(params, response, profiler);

  }

  private OrderDto findOrder(StockNotificationDto notificationDto, Profiler profiler) {
    profiler.start("GET_ORDER_BY_ID");
    OrderDto orderDto = orderFulfillmentService.findOne(notificationDto.getRequisitionId());

    if (orderDto == null) {
      profiler.stop().log();
      throw new NotFoundException(MessageKeys.ERROR_ORDER_NOT_FOUND);
    }
    return orderDto;
  }

  Profiler getProfiler(String name, Object... entryArgs) {
    extLogger.entry(entryArgs);

    Profiler profiler = new Profiler(name);
    profiler.setLogger(extLogger);

    return profiler;
  }

}
