/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service.requisition;

import static org.apache.commons.collections4.CollectionUtils.emptyIfNull;
import static org.openlmis.common.util.RequestHelper.createUri;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import org.openlmis.common.dto.PageDto;
import org.openlmis.common.dto.requisition.BasicRequisitionDto;
import org.openlmis.common.dto.requisition.RequisitionDto;
import org.openlmis.common.dto.requisition.RequisitionLineItemDto;
import org.openlmis.common.dto.requisition.StockAdjustmentDto;
import org.openlmis.common.dto.requisition.response.LossesAndAdjustment;
import org.openlmis.common.dto.requisition.response.ProductDto;
import org.openlmis.common.dto.requisition.response.RequisitionSummaryDto;
import org.openlmis.common.service.AuthService;
import org.openlmis.common.service.BaseCommunicationService;
import org.openlmis.common.service.RequestParameters;
import org.openlmis.common.util.DynamicPageTypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Service
public class RequisitionService extends BaseCommunicationService<RequisitionDto> {

  public static final String ACCESS_TOKEN = "access_token";
  private static final Logger LOGGER = LoggerFactory.getLogger(
      RequisitionService.class);
  private final RestTemplate restTemplate = new RestTemplate();
  @Value("${requisition.url}")
  private String requisitionUrl;

  @Autowired
  private AuthService authService;

  /**
   * Initiate Tz-Facility Level systems.
   *
   * @param program   program parameter
   * @param facility  facility parameter
   * @param emergency emergency parameter
   * @param period    period parameter
   *
   * @return requisitionSummaryDto initiated facility level requisition
   */
  @SuppressWarnings("PMD.PreserveStackTrace")
  public RequisitionSummaryDto initiate(UUID program, UUID facility,
                                        Boolean emergency, UUID period) {

    if (emergency == null) {
      emergency = false;
    }

    RequisitionDto requisitionDto = postRequest(RequestParameters.init()
        .set("program", program)
        .set("facility", facility)
        .set("suggestedPeriod", period)
        .set("emergency", emergency));

    if (requisitionDto != null) {

      return processResponse(requisitionDto);

    }

    return null;

  }

  /**
   * prepare response to be returned to facility level systems.
   *
   * @param requisition requisition to be returned
   */
  private RequisitionSummaryDto processResponse(RequisitionDto requisition) {

    RequisitionSummaryDto summary = new RequisitionSummaryDto();
    summary.setRnrId(requisition.getId());
    summary.setSourceOrderId(null);
    summary.setFacilityCode(requisition.getFacility().getCode());
    summary.setFacilityName(requisition.getFacility().getName());
    summary.setProgramCode(requisition.getProgram().getCode());
    summary.setPeriodId(requisition.getProcessingPeriod().getId());
    summary.setStatus(requisition.getStatus().name());
    summary.setClientSubmittedTime(requisition.getModifiedDate().toString());
    summary.setEmergency(requisition.getEmergency());

    List<ProductDto> products = new ArrayList<>();

    for (RequisitionLineItemDto item : emptyIfNull(requisition.getRequisitionLineItems())) {

      ProductDto product = new ProductDto();
      product.setProductCode(item.getOrderable().getProductCode());
      product.setQuantityDispensed(item.getTotalConsumedQuantity());
      product.setQuantityReceived(item.getTotalReceivedQuantity());
      product.setBeginningBalance(item.getBeginningBalance());
      product.setStockInHand(item.getStockOnHand());
      product.setDosesPerDispensingUnit(item.getOrderable().getDispensable().getDispensingUnit());
      product.setStockOutDays(item.getTotalStockoutDays());
      product.setQuantityRequested(item.getRequestedQuantity());
      product.setPrice(item.getPricePerPack().getAmount());
      product.setReasonForRequestedQuantity(item.getRequestedQuantityExplanation());

      if (!item.getStockAdjustments().isEmpty()) {

        List<LossesAndAdjustment> adjustments = new ArrayList<>();

        for (StockAdjustmentDto adjustment : item.getStockAdjustments()) {

          LossesAndAdjustment adjust = new LossesAndAdjustment();

          adjust.setQuantity(adjustment.getQuantity());
          adjust.setReasonId(adjustment.getReasonId());

          adjustments.add(adjust);

        }
        product.setLossesAndAdjustments(adjustments);
      }

      products.add(product);

    }
    summary.setProducts(products);
    return summary;
  }

  /**
   * Finds requisitions matching all of the provided parameters.
   */
  public List<BasicRequisitionDto> search(RequestParameters params,
                                          boolean periodNeeded) {
    String seachApiUrl = "";

    if (periodNeeded) {
      seachApiUrl = MessageFormat.format("{0}{1}initiate",
          getServiceUrl(), getUrl());
    } else {
      seachApiUrl = MessageFormat.format("{0}{1}search",
          getServiceUrl(), getUrl());
    }
    RequestParameters parameters = RequestParameters.init()
        .setAll(params)
        .set(ACCESS_TOKEN, obtainUserAccessToken());

    ParameterizedTypeReference<PageDto<BasicRequisitionDto>> parameterizedType =
        new DynamicPageTypeReference<>(BasicRequisitionDto.class);

    PageDto<BasicRequisitionDto> page = restTemplate
        .exchange(createUri(seachApiUrl, parameters),
            HttpMethod.GET,
            null,
            parameterizedType)
        .getBody();

    return page.toList();
  }

  /**
   * Finds requisitions matching all of the provided parameters.
   */
  public RequisitionDto postRequest(RequestParameters params) {

    String url = MessageFormat.format("{0}{1}initiate",
        getServiceUrl(), getUrl());

    RequestParameters parameters = RequestParameters.init()
        .setAll(params)
        .set(ACCESS_TOKEN, authService.obtainAccessToken2());

    return restTemplate
        .postForObject(createUri(url, parameters),
            HttpMethod.POST,
            RequisitionDto.class);

  }

  /**
   * Retrieves access token from the current HTTP context.
   *
   * @return token.
   */
  public String obtainUserAccessToken() {
    HttpServletRequest request = getCurrentHttpRequest();
    if (request == null) {
      return null;
    }

    String accessToken = request.getParameter(ACCESS_TOKEN);
    return accessToken;
  }

  private HttpServletRequest getCurrentHttpRequest() {
    RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
    if (requestAttributes instanceof ServletRequestAttributes) {
      HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
      return request;
    }
    return null;
  }

  @Override
  protected String getServiceUrl() {
    return requisitionUrl;
  }

  @Override
  protected String getUrl() {
    return "/api/requisitions/";
  }

  @Override
  protected Class<RequisitionDto> getResultClass() {
    return RequisitionDto.class;
  }

  @Override
  protected Class<RequisitionDto[]> getArrayResultClass() {
    return RequisitionDto[].class;
  }

  @Override
  protected String getServiceName() {
    return "Requisition";
  }

  /**
   * Finds requisitions matching all of the provided parameters.
   *
   * @return
   */
  public List<RequisitionDto> search4(UUID facility, UUID program,
                                      Boolean emergency) {

    RequestParameters parameters = RequestParameters.init()
        .set("facilityId", facility)
        .set("programId", program)
        .set("emergency", emergency);
    return findAll("periodsForInitiate",
        parameters, null, HttpMethod.GET, RequisitionDto[].class);

  }

  /**
   * Prepare response to be submitted to external system.
   *
   * @param dbReport    previous requisition to be updated
   * @param requisition current requisition
   */
  public void submit(RequisitionDto dbReport, RequisitionSummaryDto requisition) {

    for (RequisitionLineItemDto lineItem : dbReport.getRequisitionLineItems()) {

      for (ProductDto product : requisition.getProducts()) {

        if (lineItem.getOrderable().getProductCode().equals(product.getProductCode())) {

          lineItem.setBeginningBalance(product.getBeginningBalance());
          lineItem.setTotalReceivedQuantity(product.getQuantityReceived());
          lineItem.setStockOnHand(null);
          lineItem.setTotalStockoutDays(product.getStockOutDays());
          lineItem.setRequestedQuantity(product.getQuantityRequested());
          lineItem.setTotalConsumedQuantity(product.getQuantityDispensed());
          lineItem.setRequestedQuantityExplanation(product.getReasonForRequestedQuantity());
        }

      }

    }
    prepareSubmission(dbReport);
  }

  private void prepareSubmission(RequisitionDto dbReport) {

    RequisitionDto requisition = postSubmit(RequestParameters.init()
        .set("id", dbReport.getId()), dbReport);

    LOGGER.debug("data = {}", requisition);

  }

  /**
   * Finds requisitions matching all of the provided parameters.
   *
   * @return
   */
  public RequisitionDto postSubmit(RequestParameters params,
                                   RequisitionDto requisition) {

    String url = MessageFormat.format("{0}{1}{2}", getServiceUrl(),
        getUrl(), requisition.getId());

    RequestParameters parameters = RequestParameters.init()
        .setAll(params)
        .set(ACCESS_TOKEN, authService.obtainAccessToken2());

    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

    HttpEntity<RequisitionDto> entity = new HttpEntity<>(requisition, headers);

    return restTemplate
        .exchange(createUri(url, parameters),
            HttpMethod.PUT,
            entity,
            RequisitionDto.class).getBody();
  }

}

