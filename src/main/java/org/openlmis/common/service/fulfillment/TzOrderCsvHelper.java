/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service.fulfillment;

import static java.time.format.DateTimeFormatter.ofPattern;
import static org.apache.commons.collections.CollectionUtils.filter;

import java.io.IOException;
import java.io.Writer;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.apache.commons.jxpath.JXPathContext;
import org.openlmis.common.dto.ErpOrderLineItemDto;
import org.openlmis.common.dto.VersionEntityReference;
import org.openlmis.common.dto.fulfillment.OrderDto;
import org.openlmis.common.dto.fulfillment.OrderLineItemDto;
import org.openlmis.common.dto.referencedata.FacilityDto;
import org.openlmis.common.dto.referencedata.FileColumnDto;
import org.openlmis.common.dto.referencedata.FileTemplateDto;
import org.openlmis.common.dto.referencedata.OrderableDto;
import org.openlmis.common.dto.requisition.ProcessingPeriodDto;
import org.openlmis.common.service.referencedata.FacilityReferenceDataService;
import org.openlmis.common.service.referencedata.OrderableReferenceDataService;
import org.openlmis.common.service.referencedata.PeriodReferenceDataService;
import org.slf4j.ext.XLogger;
import org.slf4j.ext.XLoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class TzOrderCsvHelper {

  private static final XLogger XLOGGER = XLoggerFactory.getXLogger(TzOrderCsvHelper.class);

  private static final String STRING = "string";

  private static final String LINE_NO = "line_no";

  private static final String ORDER = "order";

  private static final String LINE_ITEM_ORDERABLE = "lineItemOrderable";

  private static final String FACILITY = "Facility";

  private static final String PRODUCT = "Orderable";

  private static final String PERIOD = "ProcessingPeriod";

  private static final String ERP_ORDER_LINE_ITEM = "ErpOrderLineItemData";

  private static final String LINE_SEPARATOR = "\r\n";

  private static final Boolean ENCLOSE_VALUES_WITH_QUOTES = false;

  @Autowired
  private FacilityReferenceDataService facilityReferenceDataService;

  @Autowired
  private PeriodReferenceDataService periodReferenceDataService;

  @Autowired
  private OrderableReferenceDataService orderableReferenceDataService;

  @Value("${order.export.includeZeroQuantity}")
  private boolean includeZeroQuantity;

  /**
   * Exporting order to csv.
   */
  public void writeCsvFile(OrderDto order, FileTemplateDto fileTemplate, Writer writer)
      throws IOException {

    List<FileColumnDto> fileColumns = fileTemplate.getFileColumns();
    removeExcludedColumns(fileColumns);
    if (fileTemplate.getHeaderInFile()) {
      writeHeader(fileColumns, writer);
    }

    XLOGGER.info("Get reference data ");

    List<UUID> orderableIds = new ArrayList<>();

    for (OrderLineItemDto orderLineItem : order.getOrderLineItems()) {
      orderableIds.add(orderLineItem.getOrderable().getId());
    }

    List<OrderableDto> orderables = orderableReferenceDataService
        .findByIds(orderableIds);

    Map<UUID, OrderableDto> orderablesMap = orderables.stream()
        .collect(Collectors.toMap(OrderableDto::getId, Function.identity()));

    FacilityDto supplyingFacility
        = facilityReferenceDataService.findOne(order.getSupplyingFacility().getId());
    FacilityDto facilityDto
        = facilityReferenceDataService.findOne(order.getFacility().getId());
    ProcessingPeriodDto processingPeriodDto
        = periodReferenceDataService.findOne(order.getProcessingPeriod().getId());


    writeLineItems(order, order.getOrderLineItems(),
        supplyingFacility, facilityDto, processingPeriodDto, orderablesMap, fileColumns, writer);
  }

  private void removeExcludedColumns(List<FileColumnDto> fileColumns) {
    filter(fileColumns, object -> ((FileColumnDto) object).getInclude());
  }

  private void writeHeader(List<FileColumnDto> fileColumns, Writer writer)
      throws IOException {
    for (FileColumnDto column : fileColumns) {
      String columnLabel = column.getColumnLabel();
      if (columnLabel == null) {
        columnLabel = "";
      }
      writer.write(columnLabel);
      if (fileColumns.indexOf(column) == (fileColumns.size() - 1)) {
        writer.write(LINE_SEPARATOR);
        break;
      }
      writer.write(",");
    }
  }

  private void writeLineItems(OrderDto order, List<OrderLineItemDto> orderLineItems,
                              FacilityDto supplyingFacility, FacilityDto facilityDto,
                              ProcessingPeriodDto processingPeriodDto,
                              Map<UUID, OrderableDto> orderablesMap,
                              List<FileColumnDto> fileColumns, Writer writer)
      throws IOException {
    int counter = 1;

    for (OrderLineItemDto orderLineItem : orderLineItems) {
      if (includeZeroQuantity || orderLineItem.getOrderedQuantity() > 0) {

        XLOGGER.info("Populate ERP Line Item Export Data ");

        ErpOrderLineItemDto erpOrderLineItem = new ErpOrderLineItemDto(order,
            orderLineItem, orderablesMap, facilityDto,
            supplyingFacility, processingPeriodDto, counter);

        writeCsvLineItem(order, orderLineItem, orderLineItem.getOrderable(),
            fileColumns, writer, counter++, erpOrderLineItem);
        writer.write(LINE_SEPARATOR);
      }
    }
  }

  private void writeCsvLineItem(OrderDto order, OrderLineItemDto orderLineItem,
                                OrderableDto orderable,
                                List<FileColumnDto> fileColumns, Writer writer, int counter,
                                ErpOrderLineItemDto erpOrderLineItem)
      throws IOException {
    JXPathContext orderContext = JXPathContext.newContext(order);
    JXPathContext lineItemContext = JXPathContext.newContext(orderLineItem);
    JXPathContext lineItemOrderableContext = JXPathContext.newContext(orderable);

    JXPathContext erpOrderLineItemContext = JXPathContext.newContext(erpOrderLineItem);

    for (FileColumnDto fileColumn : fileColumns) {
      if (fileColumn.getNested() == null || fileColumn.getNested().isEmpty()) {
        if (fileColumns.indexOf(fileColumn) < fileColumns.size() - 1) {
          writer.write(",");
        }
        continue;
      }
      Object columnValue = getColumnValue(counter, orderContext, lineItemContext,
          lineItemOrderableContext, fileColumn, erpOrderLineItemContext);

      if (columnValue instanceof ZonedDateTime) {
        columnValue = ((ZonedDateTime) columnValue).format(ofPattern(fileColumn.getFormat()));
      } else if (columnValue instanceof LocalDate) {
        columnValue = ((LocalDate) columnValue).format(ofPattern(fileColumn.getFormat()));
      }
      if (ENCLOSE_VALUES_WITH_QUOTES) {
        writer.write("\"" + (columnValue).toString() + "\"");
      } else {
        writer.write((columnValue).toString());
      }
      if (fileColumns.indexOf(fileColumn) < fileColumns.size() - 1) {
        writer.write(",");
      }
    }
  }

  private Object getColumnValue(int counter, JXPathContext orderContext,
                                JXPathContext lineItemContext,
                                JXPathContext lineItemOrderableContext,
                                FileColumnDto fileColumn,
                                JXPathContext erpOrderLineItemContext) {
    Object columnValue;

    switch (fileColumn.getNested()) {
      case STRING:
        columnValue = fileColumn.getKeyPath();
        break;
      case LINE_NO:
        columnValue = counter;
        break;
      case ERP_ORDER_LINE_ITEM:
        columnValue = erpOrderLineItemContext.getValue(fileColumn.getKeyPath());
        break;
      case ORDER:
        columnValue = orderContext.getValue(fileColumn.getKeyPath());
        break;
      case LINE_ITEM_ORDERABLE:
        columnValue = lineItemOrderableContext.getValue(fileColumn.getKeyPath());
        break;
      default:
        columnValue = lineItemContext.getValue(fileColumn.getKeyPath());
        break;
    }

    if (fileColumn.getRelated() != null && !fileColumn.getRelated().isEmpty()) {
      if (columnValue instanceof VersionEntityReference) {
        columnValue = ((VersionEntityReference) columnValue).getId();
      }
      columnValue = getRelatedColumnValue((UUID) columnValue, fileColumn);
    }

    return columnValue == null ? "" : columnValue;
  }

  private Object getRelatedColumnValue(UUID relatedId, FileColumnDto fileColumn) {
    if (relatedId == null) {
      return null;
    }

    Object columnValue;

    switch (fileColumn.getRelated()) {
      case FACILITY:
        FacilityDto facility = facilityReferenceDataService.findOne(relatedId);
        columnValue = getValue(facility, fileColumn.getRelatedKeyPath());
        break;
      case PRODUCT:
        OrderableDto product = orderableReferenceDataService.findOne(relatedId);
        columnValue = getValue(product, fileColumn.getRelatedKeyPath());
        break;
      case PERIOD:
        ProcessingPeriodDto period = periodReferenceDataService.findOne(relatedId);
        columnValue = getValue(period, fileColumn.getRelatedKeyPath());
        break;
      default:
        columnValue = null;
        break;
    }

    return columnValue;
  }

  private Object getValue(Object object, String keyPath) {
    JXPathContext context = JXPathContext.newContext(object);

    return context.getValue(keyPath);
  }

}
