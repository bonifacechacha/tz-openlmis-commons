/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service.fulfillment;

import static org.openlmis.common.util.RequestHelper.createEntity;
import static org.openlmis.common.util.RequestHelper.createUri;

import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.openlmis.common.dto.StatusMessageDto;
import org.openlmis.common.dto.fulfillment.OrderDto;
import org.openlmis.common.dto.fulfillment.StatusChangeDto;
import org.openlmis.common.dto.requisition.RequisitionDto;
import org.openlmis.common.exception.ValidationMessageException;
import org.openlmis.common.i18n.MessageKeys;
import org.openlmis.common.service.RequestParameters;
import org.openlmis.common.service.referencedata.UserReferenceDataService;
import org.openlmis.common.service.requisition.RequisitionService;
import org.openlmis.common.util.AuthenticationHelper;
import org.openlmis.common.util.Message;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

@Service
@RequiredArgsConstructor
public class OrderFulfillmentService extends BaseFulfillmentService<OrderDto> {

  private static final String JR_TEMPLATE_ORDER_PRINT =
      "/jasperTemplates/OrderPrintJRTemplate.jrxml";

  private final RequisitionService requisitionService;

  private final AuthenticationHelper authenticationHelper;

  private final UserReferenceDataService userReferenceDataService;

  @Value("${groupingSeparator}")
  private String groupingSeparator;

  @Value("${groupingSize}")
  private String groupingSize;

  @Value("${dateTimeFormat}")
  private String dateTimeFormat;

  /**
   * Print order with specified order id as a PDF on the provided output stream.
   *
   * @param orderId      the id of the order to print.
   * @param outputStream the output stream for printing the order.
   *
   * @throws JRException when there is an error caused in the processing of JR template.
   */
  public void printOrder(UUID orderId, OutputStream outputStream) throws JRException {
    OrderDto order = findOne(orderId);
    printOrder(order, outputStream);
  }

  /**
   * Print specified orderas a PDF on the provided output stream.
   *
   * @param order        the order to print.
   * @param outputStream the output stream for printing the order.
   *
   * @throws JRException when there is an error caused in the processing of JR template.
   */
  public void printOrder(OrderDto order, OutputStream outputStream) throws JRException {
    for (StatusChangeDto statusChange : order.getStatusChanges()) {
      statusChange.setAuthor(userReferenceDataService.findOne(statusChange.getAuthorId()));
    }

    Map<String, Object> params = new HashMap<>();
    DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
    decimalFormatSymbols.setGroupingSeparator(groupingSeparator.charAt(0));
    DecimalFormat decimalFormat = new DecimalFormat("", decimalFormatSymbols);
    decimalFormat.setGroupingSize(Integer.parseInt(groupingSize));
    params.put("decimalFormat", decimalFormat);
    params.put("dateTimeFormat", dateTimeFormat);
    params.put("order", order);
    params.put("loggedInUser", authenticationHelper.getCurrentUser().printName());

    RequisitionDto requisition = requisitionService.findOne(order.getExternalId());
    List<StatusMessageDto> statusMessages = requisition.getStatusHistory()
        .stream()
        .filter(status -> status.getStatusMessageDto() != null)
        .map(status -> status.getStatusMessageDto())
        .sorted(Comparator.comparing(message -> message.getCreatedDate()))
        .collect(Collectors.toList());
    params.put("statusMessages", statusMessages);
    params.put("requisition", requisition);

    params.put("program", order.getProgram().getName());
    params.put("year", String.valueOf(order.getCreatedDate().getYear()));
    params.put("schedule", order.getProcessingPeriod().getProcessingSchedule().getName());
    params.put("period", order.getProcessingPeriod().getName());


    //put data sources as params
    params.put(
        "orderLineItemsDataSource",
        new JRBeanCollectionDataSource(order.getOrderLineItems())
    );
    params.put(
        "requisitionStatusMessagesDataSource",
        new JRBeanCollectionDataSource(statusMessages)
    );

    InputStream stream = this.getClass().getResourceAsStream(JR_TEMPLATE_ORDER_PRINT);

    JasperReport jasperReport = JasperCompileManager.compileReport(stream);

    JasperPrint jasperPrint = JasperFillManager.fillReport(
        jasperReport, params, new JREmptyDataSource()
    );

    JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);
  }

  /**
   * Creates a new instance of order multiple orders by posting to the
   * batch order creation endpoint.
   *
   * @param orders list of orders to create
   */
  public void create(List<OrderDto> orders) {
    try {
      String url = getServiceUrl() + getBatchUrl();
      HttpEntity<List<OrderDto>> body = createEntity(orders, authService.obtainAccessToken());
      postNew(url, body);
    } catch (RestClientException ex) {
      throw new ValidationMessageException(
          ex, new Message(MessageKeys.ERROR_CONVERTING_MULTIPLE_REQUISITIONS));
    }
  }

  /**
   * Finds orders that matched the provided parameters.
   */
  public List<OrderDto> search(UUID supplyingFacility, UUID requestingFacility,
                               UUID program, UUID processingPeriod, String status) {
    RequestParameters parameters = RequestParameters
        .init()
        .set("supplyingFacilityId", supplyingFacility)
        .set("requestingFacilityId", requestingFacility)
        .set("programId", program)
        .set("processingPeriodId", processingPeriod)
        .set("status", status);

    return getPage("", parameters).getContent();
  }

  private void postNew(String url, HttpEntity<?> body) {
    restTemplate.postForEntity(createUri(url), body, Object.class);
  }

  protected String getBatchUrl() {
    return getUrl() + "/batch";
  }

  @Override
  protected String getUrl() {
    return "/api/orders/";
  }

  @Override
  protected Class<OrderDto> getResultClass() {
    return OrderDto.class;
  }

  @Override
  protected Class<OrderDto[]> getArrayResultClass() {
    return OrderDto[].class;
  }

}