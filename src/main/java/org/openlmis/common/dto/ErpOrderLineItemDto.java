/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.dto;

import java.util.Map;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.openlmis.common.dto.fulfillment.OrderDto;
import org.openlmis.common.dto.fulfillment.OrderLineItemDto;
import org.openlmis.common.dto.referencedata.FacilityDto;
import org.openlmis.common.dto.referencedata.OrderableDto;
import org.openlmis.common.dto.requisition.ProcessingPeriodDto;

@NoArgsConstructor
@AllArgsConstructor
public class ErpOrderLineItemDto {

  private static final String COMPANY_FIELD_VALUE = "MSD-FIN";
  private static final String CURRENCY_FIELD_VALUE = "TZS";
  private static final String UOM_FIELD = "uom";
  private static final String PRODUCT_CODE_FIELD = "productCode";
  private static final String REVISION_FIELD = "revision";

  @Setter
  @Getter
  public String plant;

  @Setter
  @Getter
  public String company;

  @Setter
  @Getter
  public String facilityId;

  @Setter
  @Getter
  public String revision;

  @Setter
  @Getter
  public String uom;

  @Setter
  @Getter
  public String currency;

  @Setter
  @Getter
  public String period;

  @Setter
  @Getter
  public String quoteDate;

  @Setter
  @Getter
  public String description;

  @Setter
  @Getter
  public String price;

  @Setter
  @Getter
  public String error;

  @Setter
  @Getter
  public String quoteNumber;

  @Setter
  @Getter
  public String orderLine;

  @Setter
  @Getter
  public String status;

  @Setter
  @Getter
  public String priceListCode;

  @Setter
  @Getter
  public String quoteLine;

  @Setter
  @Getter
  public String supplyingFacilityName;

  @Setter
  @Getter
  public boolean externallyFulfilled;

  @Setter
  @Getter
  public String customerId;

  @Setter
  @Getter
  public String productCode;

  @Setter
  @Getter
  public String orderNumber;

  @Setter
  @Getter
  public String approvedQuantity;

  /**
   * Create new ErpOrderLineItemDto object based on object of {@link OrderDto}.
   *
   * @param order               Object of order
   * @param orderLineItem       Object of order line item
   * @param orderableDtoMap     Object of orderable line
   * @param facilityDto         Object of Facilities
   * @param supplyingFacility   Object of Facility
   * @param processingPeriodDto Object of processing period
   * @param counter             number of order line items
   */
  public ErpOrderLineItemDto(OrderDto order, OrderLineItemDto orderLineItem,
                             Map<UUID, OrderableDto> orderableDtoMap, FacilityDto facilityDto,
                             FacilityDto supplyingFacility,
                             ProcessingPeriodDto processingPeriodDto, int counter) {
    this.plant = supplyingFacility.getCode();
    this.company = COMPANY_FIELD_VALUE;
    this.orderNumber = order.getOrderCode();
    String extraDataProductCode = orderableDtoMap
        .get(orderLineItem.getOrderable().getId()).getExtraData()
        .getOrDefault(PRODUCT_CODE_FIELD, null);

    if (extraDataProductCode != null) {
      this.productCode = extraDataProductCode;
      this.uom = orderableDtoMap
          .get(orderLineItem.getOrderable().getId()).getExtraData()
          .getOrDefault(UOM_FIELD, null);
      this.revision = orderableDtoMap
          .get(orderLineItem.getOrderable().getId()).getExtraData()
          .getOrDefault(REVISION_FIELD, null);
    } else {
      this.productCode = orderableDtoMap
          .get(orderLineItem.getOrderable().getId()).getProductCode();
    }
    this.customerId = facilityDto.getCode();
    this.currency = CURRENCY_FIELD_VALUE;
    this.approvedQuantity = orderLineItem.getOrderedQuantity().toString();
    this.period = processingPeriodDto.getStartDate();
    //.format(DateTimeFormatter.ISO_LOCAL_DATE);
    this.orderLine = String.valueOf(counter);
    this.status = order.getStatus().toString();

  }

}
