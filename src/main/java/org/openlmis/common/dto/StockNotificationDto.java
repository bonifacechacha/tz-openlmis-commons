/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.dto;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.openlmis.common.domain.StockNotification;

@Getter
@Setter
@NoArgsConstructor
public class StockNotificationDto
    implements StockNotification.Importer, StockNotification.Exporter {

  private UUID requisitionId;

  private String quoteNumber;

  private String customerId;

  private String customerName;

  private String hfrCode;

  private String elmisOrderNumber;

  private String notificationDate;

  private String processingDate;

  private String zone;

  private String comment;

  private UUID id;

  private List<StockNotificationLineItemDto> lineItems;

  /**
   * Creates new set of StockNotificationDto based on
   * {@link StockNotification} iterable.
   */
  public static Set<StockNotificationDto> newInstance(
      Iterable<StockNotification> iterable) {
    Set<StockNotificationDto> notificationDtos = new HashSet<>();
    iterable.forEach(i -> notificationDtos.add(newInstance(i)));
    return notificationDtos;
  }

  /**
   * Create new instance of StockNotificationDto based on given
   * {@link StockNotification}.
   *
   * @param stock instance of parameter lineItem.
   *
   * @return new instance of StockNotificationDto.
   */
  public static StockNotificationDto newInstance(
      StockNotification stock
  ) {
    StockNotificationDto dto = new StockNotificationDto();
    stock.export(dto);

    if (stock.getLineItems() != null) {
      dto.setLineItems(
          stock.getLineItems()
              .stream()
              .map(StockNotificationLineItemDto::newInstance)
              .collect(Collectors.toList())
      );
    }
    return dto;
  }

}
