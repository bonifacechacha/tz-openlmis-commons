/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.dto.requisition;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.openlmis.common.dto.referencedata.FacilityDto;
import org.openlmis.common.dto.referencedata.OrderableDto;
import org.openlmis.common.dto.referencedata.ProgramDto;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RequisitionDto extends RequisitionPeriodDto {

  private ZonedDateTime createdDate;

  private ZonedDateTime modifiedDate;

  private RequisitionStatus status;

  private Boolean emergency;

  private UUID supervisoryNode;

  private Map<String, Object> extraData;
  private FacilityDto facility;

  private ProgramDto program;
  private ProcessingPeriodDto processingPeriod;
  private List<RequisitionLineItemDto> requisitionLineItems;

  private Set<OrderableDto> availableFullSupplyProducts;

  private Set<OrderableDto> availableNonFullSupplyProducts;

  private BigDecimal allocatedBudget;

  private String sourceApplication;

  private UUID supplyingFacility;

  private Map<String, StatusLogEntry> statusChanges;

  private List<StatusChange> statusHistory;

  private LocalDate datePhysicalStockCountCompleted;

  private List<ReasonDto> stockAdjustmentReasons;


}
