/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.dto.requisition;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.openlmis.common.dto.BaseDto;
import org.openlmis.common.dto.referencedata.FacilityDto;
import org.openlmis.common.dto.referencedata.OrderableDto;
import org.openlmis.common.dto.referencedata.ProgramDto;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RequisitionDtoV2 extends BaseDto {

  private String sourceOrderId;
  private String sourceApplication;

  private ZonedDateTime createdDate;

  private String createdDate2;

  private String draftStatusMessage;

  private String status;

  private Boolean emergency;

  private FacilityDto facility;

  private ProgramDto program;

  private ProcessingPeriodDto processingPeriod;

  private BasicRequisitionTemplateDto template;

  private List<RequisitionLineItemDto> requisitionLineItems;

  private Map<String, StatusLogEntry> statusChanges = new HashMap<>();

  private List<StatusChangeDto> statusHistory = new ArrayList<>();

  private List<ReasonDto> stockAdjustmentReasons;

  private LocalDate datePhysicalStockCountCompleted;

  private Map<String, Object> extraData;

  private Boolean reportOnly;

  private UUID supplyingFacility;

  private UUID supervisoryNode;

  private Set<OrderableDto> availableFullSupplyProducts;

  private Set<OrderableDto> availableNonFullSupplyProducts;

  public String getCreatedDate() {
    return this.createdDate.toOffsetDateTime().toString();
  }

  public String setCreatedDate() {
    return this.createdDate.toOffsetDateTime().toString();
  }

}
