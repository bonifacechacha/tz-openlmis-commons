DROP TABLE IF EXISTS common.stock_notification_line_items;

DROP TABLE IF EXISTS common.stock_notifications;

CREATE TABLE common.stock_notifications (

                                            id uuid NOT NULL,
                                            quoteNumber TEXT,
                                            customerId TEXT,
                                            customerName TEXT NOT NULL,
                                            hfrCode TEXT,
                                            elmisOrderNumber TEXT,
                                            notificationDate TEXT,
                                            processingDate TEXT,
                                            zone TEXT,
                                            comment TEXT,
                                            requisitionId uuid NOT NULL

);

--
-- Name: stock_notifications_pkey; Type: CONSTRAINT; Schema: common; Owner: postgres; Tablespace:
--

ALTER TABLE ONLY common.stock_notifications
    ADD CONSTRAINT stock_notifications_pkey PRIMARY KEY (id);


CREATE TABLE common.stock_notification_line_items (

                                                      id uuid NOT NULL,
                                                      itemCode text,
                                                      itemDescription text,
                                                      uom text,
                                                      quantity INTEGER,
                                                      quantityShipped INTEGER,
                                                      quantityOrdered INTEGER,
                                                      missingItemStatus TEXT,
                                                      dueDate TEXT,
                                                      notificationId UUID NOT NULL
                                                  );

--
-- Name: stock_notification_line_items_pkey; Type: CONSTRAINT; Schema: common; Owner: postgres; Tablespace:
--

ALTER TABLE ONLY common.stock_notification_line_items
    ADD CONSTRAINT stock_notification_line_items_pkey PRIMARY KEY (id);


--
-- Name: stock_notification_line_items fkpxphnoksec55h63evgb3obfxq; Type: FK CONSTRAINT; Schema: common; Owner: postgres
--

ALTER TABLE ONLY common.stock_notification_line_items
    ADD CONSTRAINT fk_notificationId_stock_notifications FOREIGN KEY (notificationId) REFERENCES common.stock_notifications(id);



