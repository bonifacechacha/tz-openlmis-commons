DROP TABLE IF EXISTS integration_logs;

CREATE TABLE integration_logs (

                                  id uuid NOT NULL,
                                  sourceTransactionId text,
                                  destinationId UUID,
                                  integrationName text,
                                  errorMessage text,
                                  resolved boolean default true,
                                  errorCode integer
);

--
-- Name: integration_logs_pkey; Type: CONSTRAINT; Schema: common; Owner: postgres; Tablespace:
--

ALTER TABLE ONLY integration_logs
    ADD CONSTRAINT integration_logs_pkey PRIMARY KEY (id);