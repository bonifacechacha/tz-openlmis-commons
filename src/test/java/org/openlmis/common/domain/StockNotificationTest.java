/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.domain;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.openlmis.common.ToStringTestUtils;
import org.openlmis.common.dto.StockNotificationLineItemDto;
import org.openlmis.common.exception.ValidationMessageException;
import org.openlmis.common.i18n.MessageKeys;

public class StockNotificationTest {

  @Rule
  public ExpectedException expected = ExpectedException.none();

  private UUID id = UUID.randomUUID();
  private UUID requisitionId = UUID.randomUUID();
  private String quoteNumber = RandomStringUtils.randomAlphanumeric(10);
  private String customerId = RandomStringUtils.randomAlphanumeric(25);
  private String customerName = RandomStringUtils.randomAlphanumeric(15);
  private String hfrCode = RandomStringUtils.randomAlphanumeric(7);
  private String elmisOrderNumber = RandomStringUtils.randomAlphanumeric(6);
  private String notificationDate = LocalDate.now().toString();
  private String processingDate = String.valueOf(LocalDate.now().minusDays(1));
  private String zone = RandomStringUtils.randomAlphanumeric(2);
  private String comment = "Some Comments";

  private UUID lineItemId = UUID.randomUUID();

  private String itemCode = "sOME CODEE";

  private String itemDescription = "sOME dESC";

  private String uom = "uOMS";

  private Integer quantity = 189;

  private Integer quantityShipped = 1889;

  private Integer quantityOrdered = 0;

  private String missingItemStatus = "Some missed items";

  private String dueDate = "2020-01-01";

  private List<StockNotificationLineItem> lineItems =
      Collections.singletonList(new StockNotificationLineItemDataBuilder()
          .withId(lineItemId)
          .withQuantityShipped(200)
          .build());

  @Test
  public void shouldCreateInstanceBasedOnImporter() {

    StockNotification expected = createStockNotification();

    DummyStockNotificationDto stockNotificationDto = new DummyStockNotificationDto(

        id, requisitionId, quoteNumber, customerId, customerName, hfrCode,
        elmisOrderNumber, notificationDate,
        processingDate, zone, comment,
        Collections.singletonList(new StockNotificationLineItemDto(lineItemId,
            itemCode, itemDescription, uom, quantity, quantityShipped,
            quantityOrdered, missingItemStatus, dueDate))

    );

    StockNotification actual = StockNotification.newInstance(stockNotificationDto);

    assertThat(expected, equalTo(actual));

  }

  @Test
  public void shouldThrowExceptionIfLineItemsAreNotGiven() {
    expected.expect(ValidationMessageException.class);
    expected.expectMessage(MessageKeys.ERROR_LINE_ITEM_REQUIRED);

    DummyStockNotificationDto notificationDto =
        new DummyStockNotificationDto(id, requisitionId,
            quoteNumber, customerId, customerName,
            hfrCode, elmisOrderNumber, notificationDate,
            processingDate, zone, comment, Collections.emptyList());

    StockNotification.newInstance(notificationDto);

  }

  @Test
  public void shouldExportValues() {
    DummyStockNotificationDto dummyStockNotificationDto = new DummyStockNotificationDto();

    StockNotification notification = createStockNotification();
    notification.export(dummyStockNotificationDto);

    assertEquals(id, dummyStockNotificationDto.getId());
  }

  private StockNotification createStockNotification() {

    return new StockNotificationDataBuilder()
        .withId(id)
        .withRequisitionId(requisitionId)
        .withQuoteNumber(quoteNumber)
        .withCustomerName(customerName)
        .withCustomerId(customerId)
        .withHfrCode(hfrCode)
        .withElmisOrderNumber(elmisOrderNumber)
        .withNotificationDate(notificationDate)
        .withProcessingDate(processingDate)
        .withZone(zone)
        .withComment(comment)
        .withRequisitionId(requisitionId)
        .withLineItems(lineItems)
        .build();
  }

  @Test
  public void shouldImplementToString() {
    StockNotification stockNotification = new StockNotificationDataBuilder().build();
    ToStringTestUtils.verify(StockNotification.class, stockNotification);
  }

}
