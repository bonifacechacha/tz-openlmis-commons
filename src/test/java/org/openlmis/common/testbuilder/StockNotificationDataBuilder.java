/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.testbuilder;

import java.time.LocalDate;
import java.util.UUID;
import org.apache.commons.lang.RandomStringUtils;
import org.openlmis.common.domain.StockNotification;

public class StockNotificationDataBuilder {

  private UUID id = UUID.randomUUID();
  private String quoteNumber = RandomStringUtils.randomAlphanumeric(10);
  private String customerId = RandomStringUtils.randomAlphanumeric(25);
  private String customerName = RandomStringUtils.randomAlphanumeric(15);
  private String hfrCode = RandomStringUtils.randomAlphanumeric(7);
  private String elmisOrderNumber = RandomStringUtils.randomAlphanumeric(6);
  private String notificationDate = LocalDate.now().toString();
  private String processingDate = String.valueOf(LocalDate.now().minusDays(1));
  private String zone = RandomStringUtils.randomAlphanumeric(2);
  private String comment = "Some Comments";
  private UUID requisitionId = UUID.randomUUID();

  public StockNotificationDataBuilder withQuoteNumber(String quoteNumber) {
    this.quoteNumber = quoteNumber;
    return this;
  }

  public StockNotificationDataBuilder withCustomerName(String customerName) {
    this.customerName = customerName;
    return this;
  }

  public StockNotificationDataBuilder withCustomerId(String customerId) {
    this.customerId = customerId;
    return this;
  }

  public StockNotificationDataBuilder withElmisOrderNumber(String elmisOrderNumber) {
    this.elmisOrderNumber = elmisOrderNumber;
    return this;
  }

  public StockNotificationDataBuilder withNotificationDate(String notificationDate) {
    this.notificationDate = notificationDate;
    return this;
  }

  public StockNotificationDataBuilder withProcessingDate(String processingDate) {
    this.processingDate = processingDate;
    return this;
  }

  public StockNotificationDataBuilder withZone(String zone) {
    this.zone = zone;
    return this;
  }

  public StockNotificationDataBuilder withComment(String comment) {
    this.comment = comment;
    return this;
  }

  /**
   * Creates new instance of {@link org.openlmis.common.domain.StockNotification} without id.
   */
  public StockNotification buildAsNew() {
    StockNotification notification = StockNotification.newStockNotification(id, quoteNumber,
        customerId, customerName, hfrCode, elmisOrderNumber,
        notificationDate, processingDate, zone, comment, requisitionId);
    return notification;
  }

  /**
   * Creates new instance of {@link StockNotification}.
   */
  public StockNotification build() {
    StockNotification notification = buildAsNew();
    notification.setId(id);

    return notification;
  }

}
