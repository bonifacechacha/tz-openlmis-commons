/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.web;

import static org.junit.Assert.assertThat;

import guru.nidi.ramltester.junit.RamlMatchers;
import java.net.ConnectException;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.openlmis.common.service.fulfillment.OrderFulfillmentService;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;

@SuppressWarnings({"PMD.TooManyMethods", "PMD.UnusedPrivateField"})
public class OrderControllerIntegrationTest extends BaseWebIntegrationTest {

  private static final String RESOURCE_URL = "/api/orders";

  private static final String ID_URL = RESOURCE_URL + "/{id}";

  private static final String PRINT_URL = ID_URL + "/print-order";

  private static final String FORMAT = "format";

  @MockBean
  private OrderFulfillmentService orderFulfillmentService;

  @Before
  public void setUp() {
    mockUserAuthenticated();
  }

  @Test(expected = ConnectException.class)
  public void shouldPrintOrderAsPdf() {
    restAssured.given()
        .header(HttpHeaders.AUTHORIZATION, getTokenHeader())
        .pathParam("id", UUID.randomUUID())
        .when()
        .get(PRINT_URL)
        .then()
        .statusCode(200);

    assertThat(RAML_ASSERT_MESSAGE, restAssured.getLastReport(), RamlMatchers.hasNoViolations());
  }
}
