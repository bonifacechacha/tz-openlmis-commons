/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.common.service.fulfillment;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.UUID;
import javax.inject.Inject;
import lombok.SneakyThrows;
import org.junit.Before;
import org.openlmis.common.dto.fulfillment.OrderDto;
import org.openlmis.common.dto.referencedata.ProgramDto;
import org.openlmis.common.dto.referencedata.UserDto;
import org.openlmis.common.dto.requisition.ProcessingPeriodDto;
import org.openlmis.common.dto.requisition.ProcessingScheduleDto;
import org.openlmis.common.dto.requisition.RequisitionDto;
import org.openlmis.common.service.referencedata.UserReferenceDataService;
import org.openlmis.common.service.requisition.RequisitionService;
import org.openlmis.common.util.AuthenticationHelper;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@SpringBootTest
public class OrderFulfillmentServiceIntegrationTest {

  private UserDto user;

  @MockBean
  private RequisitionService requisitionService;

  @MockBean
  private AuthenticationHelper authenticationHelper;

  @MockBean
  private UserReferenceDataService userReferenceDataService;

  @Inject
  private OrderFulfillmentService orderFulfillmentService;

  @Before
  public void setUp() {
    user = new UserDto();
    user.setId(UUID.randomUUID());
    user.setFirstName("admin");
    user.setLastName("strator");
    user.setEmail("admin@openlmis.org");

    given(authenticationHelper.getCurrentUser()).willReturn(user);
    given(userReferenceDataService.findOne(any())).willReturn(user);

    RequisitionDto requisitionDto = new RequisitionDto();
    requisitionDto.setStatusHistory(Collections.emptyList());
    given(requisitionService.findOne(any())).willReturn(requisitionDto);
  }

  /**
   * test print order pdf.
   *
   */
  @SneakyThrows
  public void shouldPrintOrderPdfSuccessfully() {
    OutputStream out = new ObjectOutputStream(new ByteArrayOutputStream());
    OrderDto order = createOrderDto();
    orderFulfillmentService.printOrder(order, out);
  }

  private OrderDto createOrderDto() {
    OrderDto orderDto = new OrderDto();
    orderDto.setStatusChanges(Collections.emptyList());
    orderDto.setExternalId(UUID.randomUUID());
    orderDto.setCreatedDate(ZonedDateTime.now());
    orderDto.setOrderLineItems(Collections.emptyList());

    ProgramDto programDto = new ProgramDto();
    programDto.setName("Test Program");
    orderDto.setProgram(programDto);

    ProcessingScheduleDto scheduleDto = new ProcessingScheduleDto();
    scheduleDto.setName("Test Schedule");

    ProcessingPeriodDto periodDto = new ProcessingPeriodDto();
    periodDto.setProcessingSchedule(scheduleDto);
    periodDto.setName("Test Period");
    orderDto.setProcessingPeriod(periodDto);

    return orderDto;
  }


}
